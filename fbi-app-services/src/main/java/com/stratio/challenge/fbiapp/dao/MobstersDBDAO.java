package com.stratio.challenge.fbiapp.dao;

import com.stratio.challenge.fbiapp.model.Mobster;
import reactor.core.publisher.Mono;

public interface MobstersDBDAO {

    Mono<Mobster> getMobsterById(String mobsterNickname);

    Mono<Mobster> getBossById(String bossNickname);

    Mono<Mobster> imprison(String bossNickname);

    Mono<Mobster> release(String bossNickname);
}



